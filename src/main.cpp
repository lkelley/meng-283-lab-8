/**
 * Simple proportional controller to regulate the position of a weight
 * on a linear actuator controlled by a DC motor based on the position
 * of an input setpoint potentiometer.
 * 
 * Written for Arduino Uno
 * by Terence Kelley and Noah Westwood
 */

#include <Arduino.h>

int setPoint;
int currentPos;
unsigned short dutyCycle;
unsigned short error;
unsigned short gain = 6;

void setup() {
  // Set pin modes
  pinMode(3, OUTPUT);
  pinMode(13, OUTPUT);

  // Default PWM to low
  digitalWrite(3, LOW);

  // Start serial comms
  Serial.begin(9600);

  // Print data output header
  Serial.println("SP\tFB\tE\tP_GAIN\tDUTY");

  // Pin 13 LED flash
  for (int i = 1; i < 2; i++) {
    digitalWrite(13, HIGH);
    delay(500);
    digitalWrite(13, LOW);
    delay(500);
  }
}

void loop() {
  // Read set point and current position, map to 8-bit
  setPoint = map(analogRead(A1), 0, 1023, 0, 255);
  currentPos = map(analogRead(A0), 0, 1023, 0, 255);

  // Calculate the error
  error = constrain(setPoint - currentPos, 0, 255);

  // Calculate duty cycle, constrained between 5 and 250
  dutyCycle = constrain(gain * error, 5, 250);

  // Set output DC
  analogWrite(3, dutyCycle);

  // Write vars to serial
  Serial.print(setPoint);
  Serial.print("\t");
  Serial.print(currentPos);
  Serial.print("\t");
  Serial.print(error);
  Serial.print("\t");
  Serial.print(gain);
  Serial.print("\t");
  Serial.print(dutyCycle);
  Serial.println("\t");

  // Sampling rate delay
  delay(20);
}
